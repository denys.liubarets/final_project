﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Cardfile.Business.Interfaces;
using Cardfile.Business.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Cardfile.Business.Identity
{
    public class JwtManager : IJwtManager
    {
        private readonly IOptions<AuthOptions> authOptions;

        public JwtManager(IOptions<AuthOptions> authOptions)
        {
            this.authOptions = authOptions;
        }

        public string GenerateToken(AccountModel accountModel)
        {
            var authParams = authOptions.Value;
            var securityKey = authParams.GetSymmetricSecurityKey();
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.NameId, accountModel.Id.ToString()),
                new Claim(ClaimTypes.Name, accountModel.UserName),
                new Claim(JwtRegisteredClaimNames.Sub, accountModel.UserName),
                new Claim(JwtRegisteredClaimNames.Email, accountModel.Email),
                new Claim("role", accountModel.Role.ToString())
            };

            var token = new JwtSecurityToken(
                authParams.Issuer, 
                authParams.Audience, 
                claims,
                expires: DateTime.Now.AddSeconds(authParams.TokenLifetime), 
                signingCredentials: credentials);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }
    }
}