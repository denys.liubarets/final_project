﻿using System.Threading.Tasks;
using Cardfile.Business.Models;

namespace Cardfile.Business.Interfaces
{
    public interface IAccountService : IService<AccountModel>
    {
        AccountModel Authenticate(AuthRequest model);
    }
}