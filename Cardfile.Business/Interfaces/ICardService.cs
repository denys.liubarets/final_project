﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Cardfile.Business.Models;

namespace Cardfile.Business.Interfaces
{
    public interface ICardService : IService<CardModel>
    {
        Task<IEnumerable<CardModel>> AddAsync(IEnumerable<CardModel> models);
        
        Task<CardModel> GetByTitleAsync(string title);

        Task<IEnumerable<CardModel>> GetByYearAsync(int year);

        Task<IEnumerable<CardModel>> GetByCategoryName(string categoryName);

        Task<IEnumerable<CardModel>> GetByLocationName(string locationName);

        IEnumerable<CardModel> GetByCountryName(string countryName);

        Task<IEnumerable<CardModel>> GetByRegionName(string regionName);
        
        Task<IEnumerable<CardModel>> GetByAccountId(int id);
    }
}