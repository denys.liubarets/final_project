﻿using Cardfile.Business.Models;

namespace Cardfile.Business.Interfaces
{
    public interface IJwtManager
    {
        string GenerateToken(AccountModel accountModel);
    }
}