﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cardfile.Business.Interfaces
{
    public interface IService<TModel> where TModel : class
    {
        Task<TModel> AddAsync(TModel model);

        Task<TModel> UpdateAsync(TModel model);

        Task DeleteByIdAsync(int id);

        IEnumerable<TModel> GetAll();
        
        Task<IEnumerable<TModel>> GetAllAsync();

        IEnumerable<TModel> GetAllByIds(IEnumerable<int> Ids);

        Task<TModel> GetByIdAsync(int id);
    }
}