﻿using System.Collections.Generic;
using Cardfile.Data.Enums;

namespace Cardfile.Business.Models
{
    public class AccountModel
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
        public ICollection<int> CardIds { get; set; }
    }
}