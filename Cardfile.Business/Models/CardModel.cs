﻿using System;

namespace Cardfile.Business.Models
{
    public class CardModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImageUrl { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public string CategoryName { get; set; }
        public string LocationName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string CountryName { get; set; }
        public string RegionName { get; set; }
        public int AccountId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public DateTime UpdatedAt { get; set; }
    }
}