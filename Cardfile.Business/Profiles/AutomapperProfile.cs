﻿using System.Linq;
using AutoMapper;
using Cardfile.Business.Models;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;

namespace Cardfile.Business.Profiles
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Account, AccountModel>()
                .ForMember(model => model.CardIds,
                    opt => opt.MapFrom(account => account.Cards.Select(card => card.Id)))
                .ReverseMap();

            CreateMap<Card, CardModel>()
                .ForMember(model => model.CategoryName, opt => opt.MapFrom(card => card.Category.Name))
                .ForMember(model => model.LocationName, opt => opt.MapFrom(card => card.Location.Name))
                .ForMember(model => model.Longitude, opt => opt.MapFrom(card => card.Location.Longitude))
                .ForMember(model => model.Latitude, opt => opt.MapFrom(card => card.Location.Latitude))
                .ForMember(model => model.CountryName, opt => opt.MapFrom(card => card.Country.Name))
                .ForMember(model => model.RegionName, opt => opt.MapFrom(card => card.Region.Name))
                .ForMember(model => model.AccountId, opt => opt.MapFrom(card => card.Account.Id))
                .ForMember(model => model.CreatedBy,
                    opt => opt.MapFrom(card => $"{card.Account.FirstName} {card.Account.LastName}"))
                .ForMember(model => model.CreatedAt, opt => opt.MapFrom(card => card.Created))
                .ForMember(model => model.UpdatedAt, opt => opt.MapFrom(card => card.Updated))
                .ReverseMap();

            CreateMap<CardModel, Category>()
                .ForMember(category => category.Name, opt => opt.MapFrom(model => model.CategoryName));
            CreateMap<CardModel, Location>()
                .ForMember(location => location.Name, opt => opt.MapFrom(model => model.LocationName));
            CreateMap<CardModel, Location>()
                .ForMember(location => location.Longitude, opt => opt.MapFrom(model => model.Longitude));
            CreateMap<CardModel, Location>()
                .ForMember(location => location.Latitude, opt => opt.MapFrom(model => model.Latitude));
            CreateMap<CardModel, Country>()
                .ForMember(country => country.Name, opt => opt.MapFrom(model => model.CountryName));
            CreateMap<CardModel, Region>()
                .ForMember(region => region.Name, opt => opt.MapFrom(model => model.RegionName));
        }
    }
}