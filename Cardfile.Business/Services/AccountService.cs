﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cardfile.Business.Interfaces;
using Cardfile.Business.Models;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;

namespace Cardfile.Business.Services
{
    public class AccountService : IAccountService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;
        
        public AccountService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        
        public AccountModel Authenticate(AuthRequest request)
        {
            var account = unitOfWork.AccountRepository.GetByCredentials(request.UserName, request.Password);
            return mapper.Map<AccountModel>(account);
        }

        public async Task<AccountModel> AddAsync(AccountModel model)
        {
            var account = mapper.Map<Account>(model);
            await unitOfWork.AccountRepository.AddAsync(account);
            await unitOfWork.SaveAsync();
            return mapper.Map<AccountModel>(account);
        }

        public async Task<AccountModel> UpdateAsync(AccountModel model)
        {
            var account = mapper.Map<Account>(model);
            unitOfWork.AccountRepository.UpdateAsync(account);
            await unitOfWork.SaveAsync();
            return mapper.Map<AccountModel>(account);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.AccountRepository.DeleteByIdAsync(id);
        }

        public IEnumerable<AccountModel> GetAll()
        {
            return unitOfWork.AccountRepository.GetAllWithDetails()
                .Select(x => mapper.Map<AccountModel>(x));
        }

        public async Task<IEnumerable<AccountModel>> GetAllAsync()
        {
            var authors = await unitOfWork.AccountRepository.GetAllWithDetailsAsync();
             return authors.Select(x => mapper.Map<AccountModel>(x));
        }

        public IEnumerable<AccountModel> GetAllByIds(IEnumerable<int> Ids)
        {
             return unitOfWork.AccountRepository.GetAllWithDetails()
                 .Where(x => Ids.Contains(x.Id))
                 .Select(x => mapper.Map<AccountModel>(x));
        }

        public async Task<AccountModel> GetByIdAsync(int id)
        {
             var entity = await unitOfWork.AccountRepository.GetByIdWithDetailsAsync(id);
             return mapper.Map<AccountModel>(entity);
        }
    }
}