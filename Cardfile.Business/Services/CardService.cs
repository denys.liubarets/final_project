﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Cardfile.Business.Interfaces;
using Cardfile.Business.Models;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Business.Services
{
    public class CardService : ICardService
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CardService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }
        
        public async Task<CardModel> AddAsync(CardModel model)
        {
            var card = await Map(model);
            await unitOfWork.CardRepository.AddAsync(card);
            await unitOfWork.SaveAsync();
            return mapper.Map<CardModel>(card);
        }

        public async Task<IEnumerable<CardModel>> AddAsync(IEnumerable<CardModel> models)
        {
            var cardModels = new List<CardModel>();
            foreach (var model in models)
            {
                var cardModel = await AddAsync(model);
                cardModels.Add(cardModel);
            }

            return cardModels;
        }

        public async Task<CardModel> UpdateAsync(CardModel model)
        {
            var card = mapper.Map<Card>(model);
            card.Updated = DateTime.Now;
            unitOfWork.CardRepository.UpdateAsync(card);
            await unitOfWork.SaveAsync();
            return mapper.Map<CardModel>(card);
        }

        public async Task DeleteByIdAsync(int id)
        {
            await unitOfWork.CardRepository.DeleteByIdAsync(id);
            await unitOfWork.SaveAsync();
        }

        public IEnumerable<CardModel> GetAll()
        {
            return unitOfWork.CardRepository.GetAll()
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<IEnumerable<CardModel>> GetAllAsync()
        {
            var cards = await unitOfWork.CardRepository.GetAllWithDetailsAsync();
            return cards.Select(x => mapper.Map<CardModel>(x));
        }

        public IEnumerable<CardModel> GetAllByIds(IEnumerable<int> Ids)
        {
            return unitOfWork.CardRepository.GetAllWithDetails()
                .Where(x => Ids.Contains(x.Id))
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<CardModel> GetByIdAsync(int id)
        {
            var card = await unitOfWork.CardRepository.GetByIdWithDetailsAsync(id);
            return mapper.Map<CardModel>(card);
        }

        public async Task<CardModel> GetByTitleAsync(string title)
        {
            var site = await unitOfWork.CardRepository.GetAllWithDetails()
                .FirstOrDefaultAsync(x => x.Title == title);
            return mapper.Map<CardModel>(site);
        }

        public async Task<IEnumerable<CardModel>> GetByYearAsync(int year)
        {
            return (await unitOfWork.CardRepository.GetAllWithDetailsAsync())
                .Where(x => x.Year == year)
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<IEnumerable<CardModel>> GetByCategoryName(string categoryName)
        {
            return (await unitOfWork.CardRepository.GetAllWithDetailsAsync())
                .Where(x => x.Category.Name == categoryName)
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<IEnumerable<CardModel>> GetByLocationName(string locationName)
        {
            return (await unitOfWork.CardRepository.GetAllWithDetailsAsync())
                .Where(x => x.Location.Name == locationName)
                .Select(x => mapper.Map<CardModel>(x));
        }

        public IEnumerable<CardModel> GetByCountryName(string countryName)
        {
            return unitOfWork.CardRepository.GetAllWithDetails()
                .Where(x => x.Country.Name == countryName)
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<IEnumerable<CardModel>> GetByRegionName(string regionName)
        {
            return (await unitOfWork.CardRepository.GetAllWithDetailsAsync())
                .Where(x => x.Region.Name == regionName)
                .Select(x => mapper.Map<CardModel>(x));
        }

        public async Task<IEnumerable<CardModel>> GetByAccountId(int id)
        {
            return (await unitOfWork.CardRepository.GetAllWithDetailsAsync())
                .Where(x => x.Account.Id == id)
                .Select(x => mapper.Map<CardModel>(x));
        }

        private async Task<Card> Map(CardModel model)
        {
            var card = mapper.Map<Card>(model);
            card.Account = await unitOfWork.AccountRepository.GetByIdWithDetailsAsync(model.AccountId);
            card.Category = await unitOfWork.CategoryRepository.GetAllWithDetails()
                .SingleOrDefaultAsync(x => x.Name == model.CategoryName) ?? card.Category;
            card.Location = await unitOfWork.LocationRepository.GetAll()
                .SingleOrDefaultAsync(x => x.Name == model.LocationName) ?? card.Location;
            card.Region = await unitOfWork.RegionRepository.GetAllWithDetails()
                .SingleOrDefaultAsync(x => x.Name == model.RegionName) ?? card.Region;
            card.Country = await unitOfWork.CountryRepository.GetAllWithDetails()
                .SingleOrDefaultAsync(x => x.Name == model.CountryName) ?? card.Country;
            return card;
        }
    }
}