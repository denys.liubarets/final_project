﻿using Cardfile.Business.Models;
using Cardfile.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Business.Validation
{
    public class AccountModelValidator : AbstractValidator<AccountModel>
    {
        public AccountModelValidator(CardfileDbContext context)
        {
            RuleFor(accountModel => accountModel.UserName)
                .NotEmpty().WithMessage("Your username cannot be empty.")
                .MinimumLength(6).WithMessage("Your username length must be at least 8.")
                .MaximumLength(50).WithMessage("Your username length must not exceed 50.")
                .MustAsync(async (accountModel, value, _) =>
                {
                    return !await context.Accounts.AnyAsync(account => account.UserName == value && 
                                                                     account.Id != accountModel.Id);
                })
                .WithMessage(accountModel => $"Name {accountModel.UserName} is already taken.");

            RuleFor(accountModel => accountModel.FirstName)
                .NotEmpty()
                .MaximumLength(50);
             
            RuleFor(accountModel => accountModel.LastName)
                .NotEmpty()
                .MaximumLength(50);
            
            RuleFor(accountModel => accountModel.Email)
                .NotEmpty()
                .EmailAddress()
                .MinimumLength(3)
                .MaximumLength(320)
                .MustAsync(async (accountModel, value, _) =>
                {
                    return !await context.Accounts.AnyAsync(account => account.Email == value && 
                                                                     account.Id != accountModel.Id);
                })
                .WithMessage(accountModel => $"Email {accountModel.Email} is already in use.");
            
            RuleFor(accountModel => accountModel.Password)
                .NotEmpty().WithMessage("Your password cannot be empty.")
                .MinimumLength(8).WithMessage("Your password length must be at least 8.")
                .MaximumLength(50).WithMessage("Your password length must not exceed 50.")
                .Matches(@"[A-Z]+").WithMessage("Your password must contain at least one uppercase letter.")
                .Matches(@"[a-z]+").WithMessage("Your password must contain at least one lowercase letter.")
                .Matches(@"[0-9]+").WithMessage("Your password must contain at least one number.")
                .Matches(@"[\!\?\*\.]+").WithMessage("Your password must contain at least one (!? *.).");
        }
    }
}