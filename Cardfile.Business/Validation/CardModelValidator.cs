﻿using System;
using Cardfile.Business.Models;
using Cardfile.Data;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Business.Validation
{
    public class CardModelValidator : AbstractValidator<CardModel>
    {
        public CardModelValidator(CardfileDbContext context)
        {
            RuleFor(model => model.Title)
                .NotEmpty()
                .MaximumLength(1024)
                .MustAsync(async (model, value, _) =>
                {
                    return !await context.Cards.AnyAsync(card => card.Title == value && card.Id != model.Id);
                })
                .WithMessage(model => $"Card with title '{model.Title}' already exists.");
            
            RuleFor(model => model.Description)
                .NotEmpty()
                .MaximumLength(4096);

            RuleFor(model => model.Url)
                .NotEmpty()
                .Must(uri => Uri.TryCreate(uri, UriKind.Absolute, out _))
                .WithMessage(model => $"Url '{model.Url}' is not a well-formed uri string.");

            RuleFor(model => model.ImageUrl)
                .NotEmpty()
                .Must(uri => Uri.TryCreate(uri, UriKind.Absolute, out _))
                .WithMessage(model => $"ImageUrl '{model.ImageUrl}' is not a well-formed uri string.");

            RuleFor(model => model.Year)
                .NotEmpty()
                .Must(year => year <= DateTime.Now.Year)
                .WithMessage("Year should be a current year or a previous year.");

            RuleFor(model => model.CategoryName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(model => model.LocationName)
                .NotEmpty()
                .MaximumLength(150);

            RuleFor(model => model.CountryName)
                .NotEmpty()
                .MaximumLength(50);

            RuleFor(model => model.RegionName)
                .NotEmpty()
                .MaximumLength(150);
        }
    }
}