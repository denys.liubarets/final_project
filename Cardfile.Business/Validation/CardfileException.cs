﻿using System;
using System.Runtime.Serialization;

namespace Cardfile.Business.Validation
{
    [Serializable]
    public class CardfileException : Exception
    {
        public CardfileException()
        {
        }

        public CardfileException(string message) : base(message)
        {
        }

        public CardfileException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected CardfileException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
