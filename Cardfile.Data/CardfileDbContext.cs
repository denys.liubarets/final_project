﻿using Cardfile.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data
{
    public class CardfileDbContext : DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Card> Cards { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Region> Regions { get; set; }

        public CardfileDbContext(DbContextOptions<CardfileDbContext> options) : base(options)
        { }
    }
}