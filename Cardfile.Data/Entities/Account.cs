﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Cardfile.Data.Enums;

namespace Cardfile.Data.Entities
{
    public class Account : BaseEntity
    {
        [Required]
        public string UserName { get; set; }
        
        [Required]
        public string Password { get; set; }
        
        public string FirstName { get; set; }
        
        public string LastName { get; set; }
        
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        
        public Role Role { get; set; }
        
        public ICollection<Card> Cards { get; set; }
    }
}