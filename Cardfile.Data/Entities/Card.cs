﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Cardfile.Data.Entities
{
    public class Card : BaseEntity
    {
        [Required]
        [MaxLength(1024)]
        public string Title { get; set; }
        
        [Required]
        [MaxLength(4096)]
        public string Description { get; set; }
        
        [Required]
        [MaxLength(2048)]
        public string Url { get; set; }
        
        [MaxLength(2048)]
        public string ImageUrl { get; set; }
        
        [Required]
        public int Year { get; set; }
        
        public Category Category { get; set; }
        
        public Location Location { get; set; }
        
        public Country Country { get; set; }
        
        public Region Region { get; set; }
        
        public Account Account { get; set; }

        public DateTime Created { get; set; }
        
        public DateTime Updated { get; set; }
    }
}