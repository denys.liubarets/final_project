﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cardfile.Data.Entities
{
    public class Category : BaseEntity
    {
        [Required]
        [MaxLength(1024)]
        public string Name { get; set; }
        
        public ICollection<Card> Cards { get; set; }
    }
}
