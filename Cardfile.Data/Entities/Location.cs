﻿using System.ComponentModel.DataAnnotations;

namespace Cardfile.Data.Entities
{
    public class Location : BaseEntity
    {
        [Required]
        [MaxLength(1024)]
        public string Name { get; set; }
        
        public double Longitude { get; set; }
        
        public double Latitude { get; set; }
    }
}
