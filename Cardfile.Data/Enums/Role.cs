﻿namespace Cardfile.Data.Enums
{
    public enum Role
    {
        User, // CRUD own
        Admin // CRUD any
    }
}