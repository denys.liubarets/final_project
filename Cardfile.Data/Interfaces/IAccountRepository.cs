﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface IAccountRepository : IRepository<Account>
    {
        Account GetByCredentials(string userName, string password);
        IQueryable<Account> GetAllWithDetails();
        Task<IEnumerable<Account>> GetAllWithDetailsAsync();
        Task<Account> GetByIdWithDetailsAsync(int id);
    }
}