﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface ICardRepository : IRepository<Card>
    {
        IQueryable<Card> GetAllWithDetails();

        Task<IEnumerable<Card>> GetAllWithDetailsAsync();

        Task<Card> GetByIdWithDetailsAsync(int id);

        IQueryable<Card> GetAllByCountryIdWithDetails(int id);
        IQueryable<Card> GetAllByCountryNameWithDetails(string name);
    }
}