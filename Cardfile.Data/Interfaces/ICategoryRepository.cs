﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface ICategoryRepository : IRepository<Category>
    {
        IQueryable<Category> GetAllWithDetails();

        Task<IEnumerable<Category>> GetAllWithDetailsAsync();
    }
}