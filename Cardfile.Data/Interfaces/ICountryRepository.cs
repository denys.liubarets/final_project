﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface ICountryRepository : IRepository<Country>
    {
        IQueryable<Country> GetAllWithDetails();

        Task<IEnumerable<Country>> GetAllWithDetailsAsync();
    }
}