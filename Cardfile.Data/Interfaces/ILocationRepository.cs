﻿using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface ILocationRepository : IRepository<Location>
    {
    }
}