﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;

namespace Cardfile.Data.Interfaces
{
    public interface IRegionRepository : IRepository<Region>
    {
        IQueryable<Region> GetAllWithDetails();

        Task<IEnumerable<Region>> GetAllWithDetailsAsync();
    }
}