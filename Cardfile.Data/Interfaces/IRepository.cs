﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Cardfile.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity: class
    {
        Task<TEntity> AddAsync(TEntity entity);

        void UpdateAsync(TEntity entity);

        void DeleteAsync(TEntity entity);

        Task DeleteByIdAsync(int id);

        Task<TEntity> GetByIdAsync(int id);

        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate);
        
        IQueryable<TEntity> GetAll();

        Task<IEnumerable<TEntity>> GetAllAsync();
    }
}