﻿using System;
using System.Threading.Tasks;

namespace Cardfile.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IAccountRepository AccountRepository { get; }
        // IAuthorRepository AuthorRepository { get; }
        ICardRepository CardRepository { get; }
        ICategoryRepository CategoryRepository  { get; }
        ICountryRepository CountryRepository { get; }
        ILocationRepository LocationRepository  { get; }
        IRegionRepository RegionRepository { get; }

        Task<int> SaveAsync();
    }
}