﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_05 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Site_SiteId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Site_SiteId",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Site_Categories_CategoryId",
                table: "Site");

            migrationBuilder.DropForeignKey(
                name: "FK_Site_Countries_CountryId",
                table: "Site");

            migrationBuilder.DropForeignKey(
                name: "FK_Site_Regions_RegionId",
                table: "Site");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Site",
                table: "Site");

            migrationBuilder.RenameTable(
                name: "Site",
                newName: "Sites");

            migrationBuilder.RenameIndex(
                name: "IX_Site_RegionId",
                table: "Sites",
                newName: "IX_Sites_RegionId");

            migrationBuilder.RenameIndex(
                name: "IX_Site_CountryId",
                table: "Sites",
                newName: "IX_Sites_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Site_CategoryId",
                table: "Sites",
                newName: "IX_Sites_CategoryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Sites",
                table: "Sites",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Sites_SiteId",
                table: "Locations",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Categories_CategoryId",
                table: "Sites",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Countries_CountryId",
                table: "Sites",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Regions_RegionId",
                table: "Sites",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Sites_SiteId",
                table: "Locations");

            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Categories_CategoryId",
                table: "Sites");

            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Countries_CountryId",
                table: "Sites");

            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Regions_RegionId",
                table: "Sites");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Sites",
                table: "Sites");

            migrationBuilder.RenameTable(
                name: "Sites",
                newName: "Site");

            migrationBuilder.RenameIndex(
                name: "IX_Sites_RegionId",
                table: "Site",
                newName: "IX_Site_RegionId");

            migrationBuilder.RenameIndex(
                name: "IX_Sites_CountryId",
                table: "Site",
                newName: "IX_Site_CountryId");

            migrationBuilder.RenameIndex(
                name: "IX_Sites_CategoryId",
                table: "Site",
                newName: "IX_Site_CategoryId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Site",
                table: "Site",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Site_SiteId",
                table: "Cards",
                column: "SiteId",
                principalTable: "Site",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Site_SiteId",
                table: "Locations",
                column: "SiteId",
                principalTable: "Site",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Site_Categories_CategoryId",
                table: "Site",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Site_Countries_CountryId",
                table: "Site",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Site_Regions_RegionId",
                table: "Site",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
