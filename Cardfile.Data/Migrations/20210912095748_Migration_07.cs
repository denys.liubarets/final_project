﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_07 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locations_Sites_SiteId",
                table: "Locations");

            migrationBuilder.DropIndex(
                name: "IX_Locations_SiteId",
                table: "Locations");

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "Sites",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocationId1",
                table: "Sites",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId1",
                table: "Sites",
                column: "LocationId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Locations_LocationId1",
                table: "Sites",
                column: "LocationId1",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Locations_LocationId1",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_LocationId1",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "LocationId1",
                table: "Sites");

            migrationBuilder.CreateIndex(
                name: "IX_Locations_SiteId",
                table: "Locations",
                column: "SiteId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Locations_Sites_SiteId",
                table: "Locations",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
