﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_08 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Locations_LocationId1",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_LocationId1",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "LocationId1",
                table: "Sites");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "Locations");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId",
                table: "Sites",
                column: "LocationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_LocationId",
                table: "Sites");

            migrationBuilder.AddColumn<int>(
                name: "LocationId1",
                table: "Sites",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SiteId",
                table: "Locations",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId1",
                table: "Sites",
                column: "LocationId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Locations_LocationId1",
                table: "Sites",
                column: "LocationId1",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
