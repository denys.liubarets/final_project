﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_12 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_LocationId",
                table: "Sites");

            migrationBuilder.AlterColumn<int>(
                name: "LocationId",
                table: "Sites",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId",
                table: "Sites",
                column: "LocationId");

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Sites_LocationId",
                table: "Sites");

            migrationBuilder.AlterColumn<int>(
                name: "LocationId",
                table: "Sites",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId",
                table: "Sites",
                column: "LocationId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Sites_Locations_LocationId",
                table: "Sites",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
