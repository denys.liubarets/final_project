﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_14 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Authors_AuthorId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards");

            migrationBuilder.AlterColumn<int>(
                name: "SiteId",
                table: "Cards",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Cards",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Authors_AuthorId",
                table: "Cards",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Authors_AuthorId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards");

            migrationBuilder.AlterColumn<int>(
                name: "SiteId",
                table: "Cards",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "AuthorId",
                table: "Cards",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Authors_AuthorId",
                table: "Cards",
                column: "AuthorId",
                principalTable: "Authors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
