﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Cardfile.Data.Migrations
{
    public partial class Migration_16 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards");

            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropIndex(
                name: "IX_Cards_SiteId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "SiteId",
                table: "Cards");

            migrationBuilder.AddColumn<int>(
                name: "CategoryId",
                table: "Cards",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CountryId",
                table: "Cards",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Cards",
                maxLength: 4096,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Cards",
                maxLength: 2048,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "LocationId",
                table: "Cards",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "RegionId",
                table: "Cards",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Cards",
                maxLength: 150,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Url",
                table: "Cards",
                maxLength: 2048,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<int>(
                name: "Year",
                table: "Cards",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Cards_CategoryId",
                table: "Cards",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_CountryId",
                table: "Cards",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_LocationId",
                table: "Cards",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Cards_RegionId",
                table: "Cards",
                column: "RegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Categories_CategoryId",
                table: "Cards",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Countries_CountryId",
                table: "Cards",
                column: "CountryId",
                principalTable: "Countries",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Locations_LocationId",
                table: "Cards",
                column: "LocationId",
                principalTable: "Locations",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Regions_RegionId",
                table: "Cards",
                column: "RegionId",
                principalTable: "Regions",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Categories_CategoryId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Countries_CountryId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Locations_LocationId",
                table: "Cards");

            migrationBuilder.DropForeignKey(
                name: "FK_Cards_Regions_RegionId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_CategoryId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_CountryId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_LocationId",
                table: "Cards");

            migrationBuilder.DropIndex(
                name: "IX_Cards_RegionId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "CountryId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Description",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "LocationId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "RegionId",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Url",
                table: "Cards");

            migrationBuilder.DropColumn(
                name: "Year",
                table: "Cards");

            migrationBuilder.AddColumn<int>(
                name: "SiteId",
                table: "Cards",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryId = table.Column<int>(type: "int", nullable: true),
                    CountryId = table.Column<int>(type: "int", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", maxLength: 4096, nullable: false),
                    ImageUrl = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: true),
                    LocationId = table.Column<int>(type: "int", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(150)", maxLength: 150, nullable: false),
                    RegionId = table.Column<int>(type: "int", nullable: true),
                    Url = table.Column<string>(type: "nvarchar(2048)", maxLength: 2048, nullable: false),
                    Year = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Sites", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Sites_Categories_CategoryId",
                        column: x => x.CategoryId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sites_Countries_CountryId",
                        column: x => x.CountryId,
                        principalTable: "Countries",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sites_Locations_LocationId",
                        column: x => x.LocationId,
                        principalTable: "Locations",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Sites_Regions_RegionId",
                        column: x => x.RegionId,
                        principalTable: "Regions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cards_SiteId",
                table: "Cards",
                column: "SiteId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_CategoryId",
                table: "Sites",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_CountryId",
                table: "Sites",
                column: "CountryId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_LocationId",
                table: "Sites",
                column: "LocationId");

            migrationBuilder.CreateIndex(
                name: "IX_Sites_RegionId",
                table: "Sites",
                column: "RegionId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cards_Sites_SiteId",
                table: "Cards",
                column: "SiteId",
                principalTable: "Sites",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
