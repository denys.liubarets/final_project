﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class AccountRepository : Repository<Account>, IAccountRepository
    {
        public AccountRepository(DbContext context) : base(context)
        {
        }

        public Account GetByCredentials(string userName, string password)
        {
            return GetAllWithDetails().SingleOrDefault(x => x.UserName == userName && x.Password == password);
        }

        public IQueryable<Account> GetAllWithDetails()
        {
            return GetAll().Include(x => x.Cards);
        }

        public async Task<IEnumerable<Account>> GetAllWithDetailsAsync()
        {
            return await GetAllWithDetails().ToListAsync();
        }

        public async Task<Account> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().SingleOrDefaultAsync(x => x.Id == id);
        }
    }
}