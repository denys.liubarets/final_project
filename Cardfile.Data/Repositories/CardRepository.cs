﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class CardRepository : Repository<Card>, ICardRepository
    {
        public CardRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Card> GetAllWithDetails()
        {
            return GetAll()
                .Include(x => x.Account)
                .Include(x => x.Category)
                .Include(x => x.Country)
                .Include(x => x.Region)
                .Include(x => x.Location);
        }

        public async Task<IEnumerable<Card>> GetAllWithDetailsAsync()
        {
            return await GetAllWithDetails().ToListAsync();
        }

        public async Task<Card> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().SingleOrDefaultAsync(x => x.Id == id);
        }

        public IQueryable<Card> GetAllByCountryIdWithDetails(int id)
        {
            return GetAllWithDetails().Where(x => x.Country.Id == id);
        }

        public IQueryable<Card> GetAllByCountryNameWithDetails(string name)
        {
            return GetAllWithDetails().Where(x => x.Country.Name == name);
        }
    }
}