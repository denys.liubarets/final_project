﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class CategoryRepository : Repository<Category>, ICategoryRepository
    {
        public CategoryRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Category> GetAllWithDetails()
        {
            return GetAll().Include(x => x.Cards);
        }

        public async Task<IEnumerable<Category>> GetAllWithDetailsAsync()
        {
            return await GetAllWithDetails().ToListAsync();
        }
    }
}