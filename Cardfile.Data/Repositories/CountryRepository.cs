﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class CountryRepository : Repository<Country>, ICountryRepository
    {
        public CountryRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Country> GetAllWithDetails()
        {
            return GetAll().Include(x => x.Cards);
        }

        public async Task<IEnumerable<Country>> GetAllWithDetailsAsync()
        {
            return await GetAllWithDetails().ToListAsync();
        }
    }
}