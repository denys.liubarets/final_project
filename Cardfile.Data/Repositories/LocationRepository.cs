﻿using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class LocationRepository : Repository<Location>, ILocationRepository
    {
        public LocationRepository(DbContext context) : base(context)
        {
        }
    }
}