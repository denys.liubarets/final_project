﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Cardfile.Data.Entities;
using Cardfile.Data.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data.Repositories
{
    public class RegionRepository : Repository<Region>, IRegionRepository
    {
        public RegionRepository(DbContext context) : base(context)
        {
        }

        public IQueryable<Region> GetAllWithDetails()
        {
            return GetAll().Include(x => x.Cards);
        }

        public async Task<IEnumerable<Region>> GetAllWithDetailsAsync()
        {
            return await GetAllWithDetails().ToListAsync();
        }
    }
}