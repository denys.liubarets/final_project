﻿using System.Threading.Tasks;
using Cardfile.Data.Interfaces;
using Cardfile.Data.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CardfileDbContext context;
        private IAccountRepository accountRepository;
        private ICardRepository cardRepository;
        private ICategoryRepository categoryRepository;
        private ICountryRepository countryRepository;
        private ILocationRepository locationRepository;
        private IRegionRepository regionRepository;
        
        public UnitOfWork(DbContextOptions<CardfileDbContext> options)
        {
            context = new CardfileDbContext(options);
        }

        public IAccountRepository AccountRepository => accountRepository ??= new AccountRepository(context);
        public ICardRepository CardRepository => cardRepository ??= new CardRepository(context);
        public ICategoryRepository CategoryRepository => categoryRepository ??= new CategoryRepository(context);
        public ICountryRepository CountryRepository => countryRepository ??= new CountryRepository(context);
        public ILocationRepository LocationRepository => locationRepository ??= new LocationRepository(context);
        public IRegionRepository RegionRepository => regionRepository ??= new RegionRepository(context);
        
        public async Task<int> SaveAsync()
        {
            return await context.SaveChangesAsync();
        }
        
        public void Dispose()
        {
            context.Dispose();
        }
    }
}