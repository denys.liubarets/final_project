﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.Configuration;
using Cardfile.Business.Models;
using Cardfile.Business.Profiles;
using Cardfile.Business.Services;
using Cardfile.Data;
using Cardfile.Data.Interfaces;
using NUnit.Framework;

namespace Cardfile.Tests.BusinessTests
{
    [TestFixture]
    public class CardServiceTests
    {
        private IUnitOfWork unitOfWork;
        private CardService cardService;
        
        [SetUp]
        public void Setup()
        {
            var options = UnitTestHelper.GetUnitTestDbContextOptions();
            unitOfWork = new UnitOfWork(options);
            cardService = new CardService(unitOfWork, UnitTestHelper.CreateMapperProfile());
        }

        [Test]
        public async Task AddAsync_AddingCardWithExistingCategory_DoesNotCreateNewCategory()
        {
            var cardModel = GenerateCardModel();
            cardModel.CategoryName = "Mixed";
            await cardService.AddAsync(cardModel);

            Assert.AreEqual(3, unitOfWork.CategoryRepository.GetAllWithDetails().Count());
        }

        [Test]
        public async Task AddAsync_AddingCardWithExistingCountry_DoesNotCreateNewCountry()
        {
            var cardModel = GenerateCardModel();
            cardModel.CountryName = "Oman";
            await cardService.AddAsync(cardModel);

            Assert.AreEqual(1, unitOfWork.CountryRepository.GetAllWithDetails().Where(x => x.Name == "Oman").Select(x => x).Count());
        }

        [Test]
        public async Task AddAsync_AddingCardWithExistingRegion_DoesNotCreateNewRegion()
        {
            var cardModel = GenerateCardModel();
            cardModel.RegionName = "Arab States";
            await cardService.AddAsync(cardModel);

            Assert.AreEqual(1, unitOfWork.RegionRepository.GetAllWithDetails().Where(x => x.Name == "Arab States").Select(x => x).Count());
        }

        private CardModel GenerateCardModel()
        {
            return new CardModel
            {
                Title = "New Card 1",
                Url = "https://url.com/testurl",
                ImageUrl = "https://url.com/testurl.jpg",
                Description = "Test Description 1",
                Year = 2001,
                CategoryName = "Category 1",
                LocationName = "Location 1",
                Longitude = 10.000001,
                Latitude = 15.000002,
                CountryName = "Country 1",
                RegionName = "Region 1",
                AccountId = 1,
                CreatedBy = "John Smith",
                CreatedAt = DateTime.Now,
                UpdatedAt = DateTime.Now.AddDays(1)
            };
        }
    }
}