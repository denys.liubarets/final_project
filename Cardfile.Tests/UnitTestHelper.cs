﻿using System;
using AutoMapper;
using Cardfile.Business.Profiles;
using Cardfile.Data;
using Cardfile.Data.Entities;
using Cardfile.Data.Enums;
using Microsoft.EntityFrameworkCore;

namespace Cardfile.Tests
{
    public static class UnitTestHelper
    {
        public static DbContextOptions<CardfileDbContext> GetUnitTestDbContextOptions()
        {
            var options = new DbContextOptionsBuilder<CardfileDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using var context = new CardfileDbContext(options);
            SeedData(context);
            return options;
        }

        private static void SeedData(CardfileDbContext context)
        {
            context.Accounts.Add(new Account {Id = 1, UserName = "AgentSmith", FirstName = "John", LastName = "Smith", Email = "jsmith@mtrx.com", Role = Role.Admin});
            context.Accounts.Add(new Account {Id = 2, UserName = "NeoAnderson", FirstName = "Neo", LastName = "Anderson", Email = "neo@zeon.com", Role = Role.User});
            context.SaveChanges();

            context.Cards.AddRange(new Card
            {
                Id = 1,
                Title = "<i>Aflaj</i> Irrigation Systems of Oman",
                Url = "https://whc.unesco.org/en/list/1207",
                ImageUrl = "https://whc.unesco.org/uploads/sites/site_1207.jpg",
                Description =
                    "<p>The property includes five <em>aflaj</em> irrigation systems and is representative of some 3,000 such systems still in use in Oman. The origins of this system of irrigation may date back to AD 500, but archaeological evidence suggests that irrigation systems existed in this extremely arid area as early as 2500 BC. Using gravity, water is channelled from underground sources or springs to support agriculture and domestic use. The fair and effective management and sharing of water in villages and towns is still underpinned by mutual dependence and communal values and guided by astronomical observations. Numerous watchtowers built to defend the water systems form part of the site reflecting the historic dependence of communities on the <em>aflaj</em> system. Threatened by falling level of the underground water table, the <em>aflaj</em> represent an exceptionally well-preserved form of land use.</p>",
                Year = 2006,
                Category = new Category {Name = "Cultural"},
                Location = new Location
                {
                    Name = "Dakhiliya, Sharqiya and Batinah Regions", Longitude = 57.145151252,
                    Latitude = 5.5360555555
                },
                Country = new Country {Name = "Oman"},
                Region = new Region {Name = "Arab States"},
                Account = context.Accounts.Find(1),
                Created = DateTime.Now,
                Updated = DateTime.Now.AddDays(1)
            },
                new Card {
                Id = 2,
                Title = "<I>Sacri Monti</I> of Piedmont and Lombardy",
                Url = "https://whc.unesco.org/en/list/1068",
                ImageUrl = "https://whc.unesco.org/uploads/sites/site_1068.jpg",
                Description =
                    "<p>The nine <em>Sacri Monti</em> (Sacred Mountains) of northern Italy are groups of chapels and other architectural features created in the late 16th and 17th centuries and dedicated to different aspects of the Christian faith. In addition to their symbolic spiritual meaning, they are of great beauty by virtue of the skill with which they have been integrated into the surrounding natural landscape of hills, forests and lakes. They also house much important artistic material in the form of wall paintings and statuary.</p>",
                Year = 2003,
                Category = new Category {Name = "Mixed"},
                Location = new Location
                {
                    Name = "Regions of Lombardy and Piedmont", Longitude = 9.12515215,
                    Latitude = 19.169555556
                },
                Country = new Country {Name = "Italy"},
                Region = new Region {Name = "Europe and North America"},
                Account = context.Accounts.Find(2),
                Created = DateTime.Now,
                Updated = DateTime.Now.AddDays(2)
            },
                new Card {
                Id = 3,
                Title = "18th-Century Royal Palace at Caserta with the Park, the Aqueduct of Vanvitelli, and the San Leucio Complex",
                Url = "https://whc.unesco.org/en/list/549",
                ImageUrl = "https://whc.unesco.org/uploads/sites/site_549.jpg",
                Description =
                    "<p>The monumental complex at Caserta, created by the Bourbon king Charles III in the mid-18th century to rival Versailles and the Royal Palace in Madrid, is exceptional for the way in which it brings together a magnificent palace with its park and gardens, as well as natural woodland, hunting lodges and a silk factory. It is an eloquent expression of the Enlightenment in material form, integrated into, rather than imposed on, its natural setting.</p>",
                Year = 1997,
                Category = new Category { Name = "Natural"},
                Location = new Location
                {
                    Name = "Provinces of Caserta and Benevento, Campania", Longitude = 14.32639,
                    Latitude = 15.125125
                },
                Country = new Country {Name = "Italy"},
                Region = context.Regions.Find(2),
                Account = context.Accounts.Find(1),
                Created = DateTime.Now,
                Updated = DateTime.Now.AddDays(1)
            });
 
            context.SaveChanges();
        }
        
        public static Mapper CreateMapperProfile()
        {
            var profile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(profile));

            return new Mapper(configuration);
        }
    }
}