﻿using System.Threading.Tasks;
using Cardfile.Business.Interfaces;
using Cardfile.Business.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cardfile.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountService accountService;
        private readonly IJwtManager jwtManager;

        public AccountsController(IAccountService accountService, IJwtManager jwtManager)
        {
            this.accountService = accountService;
            this.jwtManager = jwtManager;
        }
        
        //GET: /api/accounts/
        [HttpGet]
        [Authorize (Roles = "Admin")]
        public async Task<IActionResult> GetAll()
        {
            var accounts = await accountService.GetAllAsync();
            return Ok(accounts);
        }
        
        //GET: /api/accounts/1
        [HttpGet("{id:int}")]
        [Authorize (Roles = "Admin")]
        public async Task<IActionResult> GetById(int id)
        {
            var account = await accountService.GetByIdAsync(id);
            if (account is null)
                return NotFound(new {message = $"Account with id {id} was not found."});
            return Ok(account);
        }

        //POST: /api/accounts/register/
        [HttpPost("register")]
        [AllowAnonymous]
        public async Task<IActionResult> Register([FromBody] AccountModel accountModel)
        {
            var account = await accountService.AddAsync(accountModel);
            var token = jwtManager.GenerateToken(account);
            return Ok(new {access_token = token});
        }
        
        //POST: /api/accounts/authenticate/
        [HttpPost("authenticate")]
        [AllowAnonymous]
        public IActionResult Authenticate([FromBody] AuthRequest request)
        {
            var account = accountService.Authenticate(request);

            if (account == null) 
                return Unauthorized();
            
            var token = jwtManager.GenerateToken(account);
            return Ok(new {access_token = token});
        }
    }
}