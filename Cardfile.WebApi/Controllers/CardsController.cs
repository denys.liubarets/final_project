﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Cardfile.Business.Interfaces;
using Cardfile.Business.Models;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Cardfile.WebApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Produces("application/json")]
    public class CardsController : ControllerBase
    {
        private readonly ICardService cardService;

        public CardsController(ICardService cardService)
        {
            this.cardService = cardService;
        }
        
        //GET: /api/cards/
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CardModel>>> Get()
        {
            var cards = await cardService.GetAllAsync();
            return Ok(cards);
        }
        
        //GET: /api/cards/1
        [HttpGet("{id:int}", Name = "GetCard")]
        public async Task<ActionResult<CardModel>> Get(int id)
        {
            var card = await cardService.GetByIdAsync(id);
            if (card is null)
                return NotFound();
            return Ok(card);
        }

        //GET: /api/cards/year/2001
        [HttpGet("year/{year:int}")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetByYear(int year)
        {
            var cards = await cardService.GetByYearAsync(year);
            if (!cards.Any())
                return NotFound();
            return Ok(cards);
        }
        
        //GET: /api/cards/categories/cultural/
        [HttpGet("category")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetByCategoryName([FromQuery] string name)
        {
            var cards = await cardService.GetByCategoryName(name);
            if (!cards.Any())
                return NotFound();
            return Ok(cards);
        }
        
        //GET: /api/cards/countries/ukraine/
        [HttpGet("countries/{countryName}")]
        public ActionResult<IEnumerable<CardModel>> GetByCountryName(string countryName)
        {
            var cards = cardService.GetByCountryName(countryName);
            if (!cards.Any())
                return NotFound();
            return Ok(cards);
        }
        
        //GET: /api/cards/regions/europe/
        [HttpGet("regions/{regionName}")]
        public async Task<ActionResult<IEnumerable<CardModel>>> GetByRegionName(string regionName)
        {
            var cards = await cardService.GetByRegionName(regionName);
            if (!cards.Any())
                return NotFound();
            return Ok(cards);
        }
        
        //POST: /api/cards/
        [HttpPost]
        [Authorize (Roles = "Admin, User")]
        public async Task<IActionResult> Add([FromBody] CardModel cardModel)
        {
            cardModel.AccountId = GetAccountId();
            cardModel.CreatedAt = DateTime.Now;
            var addedCardModel = await cardService.AddAsync(cardModel);
            return CreatedAtRoute("GetCard", new {addedCardModel.Id}, addedCardModel);
        }

        //POST: /api/cards/collection/
        [HttpPost("collection")]
        [Authorize (Roles = "Admin")]
        public async Task<ActionResult<IEnumerable<CardModel>>> AddCollection(
            [FromBody] List<CardModel> cardModels)
        {
            var accountId = GetAccountId();
            cardModels.ForEach(x =>
            {
                x.AccountId = accountId;
                x.CreatedAt = DateTime.Now;
            });
            var addedCards = await cardService.AddAsync(cardModels);
            return Ok(addedCards);
        }
        
        //PUT: /api/cards/1
        [HttpPut]
        [Authorize(Roles = "Admin, User")]
        public async Task<ActionResult<CardModel>> Update([CustomizeValidator(Skip = true)] CardModel cardModel)
        {
            var accountId = GetAccountId();
            var role = GetAccountRole();

            if (cardModel.AccountId != accountId && role != "Admin")
                return Unauthorized();

            var updatedCard = await cardService.UpdateAsync(cardModel);
            return CreatedAtRoute("GetCard", new {updatedCard.Id}, updatedCard);
        }

        //DELETE: /api/cards/1
        [HttpDelete("{id:int}")]
        [Authorize (Roles = "Admin, User")]
        public async Task<IActionResult> Delete(int id)
        {
            var accountId = GetAccountId();
            var role = GetAccountRole();
            var cardToDelete = await cardService.GetByIdAsync(id);
            if (cardToDelete.AccountId != accountId && role != "Admin")
                return Unauthorized();

            await cardService.DeleteByIdAsync(id);
            return Ok();
        }

        private int GetAccountId()
        {
            var idStr = User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value;
            return int.TryParse(idStr, out var id) ? id : 0;
        }

        private string GetAccountRole()
        {
            return User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Role)?.Value;
        }
    }
}